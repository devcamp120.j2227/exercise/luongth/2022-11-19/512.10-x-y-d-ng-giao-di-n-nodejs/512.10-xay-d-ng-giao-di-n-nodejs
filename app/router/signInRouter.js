const express = require('express');
const router = express.Router();
const path = require('path');
// Import middleware
const signInMiddleware = require('../middleware/signInMiddleware')

router.get("/", signInMiddleware.getDateAndUrlMiddleware, (req, res) => {
    res.sendFile(path.join(process.cwd() + "/app/view/signIn.html" ))
})

router.use(express.static(process.cwd() + '/app/view'))

module.exports = router;
