const getDateAndUrlMiddleware = (req, res, next) => {
    console.log("*Current time: ", new Date())    
    console.log("*Current URL: " + req.protocol + "://" + req.get("host") + req.originalUrl);
    next();
}

module.exports = { getDateAndUrlMiddleware }