// Import Express
const express = require('express');
// Khai báo app 
const app = express();
// Khai báo cổng chạy
const port = 8000;
// Import router
const signInrouter = require('./app/router/signInRouter') 

app.get('/', (req, res) => {
    res.json({
        message: "Test"
    })
})

app.use('/api', signInrouter)

app.listen(port, () => {
    console.log("App listening on port:", port);
})